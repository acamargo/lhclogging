import ch.cern.lhclogging.model.ImportInformation;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by alejandrocamargo on 13/4/17.
 */
public class JsonSerializerTest {


    @Test
    public void serializerTest() {

        ObjectMapper mapper = new ObjectMapper();

        ImportInformation info = new ImportInformation();

        List<String> tags = new ArrayList<>();

        //tags.add("ETC701/E91_OK");

        tags.add("EMD203/A4_I1");
        tags.add("EMD203/A4_I2");
        tags.add("EMD203/A4_I3");
        tags.add("EMD203/A4_P*");
        tags.add("EMD203/A4_Q*");
        tags.add("EMD203/A4_S*");



        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        try {

            Date day = sdf.parse("06-12-2017");

            info.setDay(day);
            info.setTagList(tags);

            String jsonInString = mapper.writeValueAsString(info);

            System.out.println(jsonInString);


        } catch (Exception e) {

        }

    }
    @Test
    public void serializerTest2() {

        ObjectMapper mapper = new ObjectMapper();

        ImportInformation info = new ImportInformation();

        List<String> tags = new ArrayList<>();

        //tags.add("ETC701/E91_OK");

        tags.add("EMD303/A4_I1");
        tags.add("EMD303/A4_I2");
        tags.add("EMD303/A4_I3");
        tags.add("EMD303/A4_P*");
        tags.add("EMD303/A4_Q*");
        tags.add("EMD303/A4_S*");



        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        try {

            Date day = sdf.parse("01-01-2016");
            Date dayEnd = sdf.parse("06-12-2017");

            info.setDay(day);
            info.setTagList(tags);

            info.setDayEnd(dayEnd);

            String jsonInString = mapper.writeValueAsString(info);

            System.out.println(jsonInString);


        } catch (Exception e) {

        }

    }

}
