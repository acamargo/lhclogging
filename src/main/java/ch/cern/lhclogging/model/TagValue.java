package ch.cern.lhclogging.model;

import java.sql.Timestamp;

/**
 * Created by alejandrocamargo on 13/4/17.
 */
public class TagValue {

    private String tagName;
    private Timestamp timestamp;
    private double value;

    public TagValue(String tagName, Timestamp timestamp, double value) {
        this.tagName = tagName;
        this.timestamp = timestamp;
        this.value = value;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
