package ch.cern.lhclogging.model;

import java.util.Date;
import java.util.List;

/**
 * Created by alejandrocamargo on 13/4/17.
 */
public class ImportInformation {

    private List<String> tagList;

    private Date day;

    private Date dayEnd;


    public List<String> getTagList() {
        return tagList;
    }

    public void setTagList(List<String> tagList) {
        this.tagList = tagList;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public Date getDayEnd() {
        return dayEnd;
    }

    public void setDayEnd(Date dayEnd) {
        this.dayEnd = dayEnd;
    }
}
