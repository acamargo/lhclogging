package ch.cern.lhclogging.controllers;

import ch.cern.lhclogging.model.ImportInformation;
import ch.cern.lhclogging.model.TagValue;
import ch.cern.lhclogging.services.LhcLoggingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.List;

/**
 * Created by alejandrocamargo on 12/4/17.
 */

@Controller
public class LhcLoggingController {

    @Autowired
    private LhcLoggingService lhcLoggingService;

    @RequestMapping(value ="/api/import", method = RequestMethod.POST)
    public @ResponseBody List<TagValue> get(@RequestBody ImportInformation info) {

        return lhcLoggingService.importDailyData(info.getTagList(), info.getDay());

    }

    @RequestMapping(value ="/api/import/interval", method = RequestMethod.POST)
    public @ResponseBody List<TagValue> getInterval(@RequestBody ImportInformation info) {

        return lhcLoggingService.importData(info.getTagList(), info.getDay(), info.getDayEnd());

    }

    @RequestMapping(value ="/api/singleImport/{tag}", method = RequestMethod.POST)
    public @ResponseBody List<TagValue> get(@RequestBody Date day, @PathVariable String tag) {

        return lhcLoggingService.importDailyData(tag, day);

    }


}
