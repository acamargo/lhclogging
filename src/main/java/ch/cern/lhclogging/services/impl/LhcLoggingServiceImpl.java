package ch.cern.lhclogging.services.impl;

import cern.accsoft.cals.extr.client.service.MetaDataService;
import cern.accsoft.cals.extr.client.service.ServiceBuilder;
import cern.accsoft.cals.extr.client.service.TimeseriesDataService;
import cern.accsoft.cals.extr.domain.core.datasource.DataLocationPreferences;
import cern.accsoft.cals.extr.domain.core.metadata.Variable;
import cern.accsoft.cals.extr.domain.core.metadata.VariableSet;
import cern.accsoft.cals.extr.domain.core.timeseriesdata.TimeseriesData;
import cern.accsoft.cals.extr.domain.core.timeseriesdata.TimeseriesDataSet;

import ch.cern.lhclogging.model.TagValue;
import ch.cern.lhclogging.services.LhcLoggingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by alejandrocamargo on 12/4/17.
 */

@Service
public class LhcLoggingServiceImpl implements LhcLoggingService {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    public static String APP_NAME = "WEB_ENERGY";

    public static String CLIENT = "EL_CO";

    private ServiceBuilder serviceBuilder;

    private MetaDataService metaService;

    private TimeseriesDataService timeseriesDataService;

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @PostConstruct
    public void init() {

        serviceBuilder = ServiceBuilder.getInstance(APP_NAME, CLIENT, DataLocationPreferences.LDB_PRO);

        metaService = serviceBuilder.createMetaService();

        timeseriesDataService = serviceBuilder.createTimeseriesService();

    }


    /**
     * Get all the values for the given day for the list of tags in the tagList
     * It can be used to import only one tag, too.
     * The tags must follow the PSEN name convention
     *
     * @param tagList
     * @param day
     * @return
     */
    public List<TagValue> importDailyData(List<String> tagList, Date day) {

        List<TagValue> tagValues = new ArrayList<>();

        String date = sdf.format(day);

        List<String> psenTags = tagList
                .stream()
                .map(t -> convertTagNameToPsenName(t))
                .collect(Collectors.toList());


        // 1. Set the init time to import and the end time
        Timestamp startTime = Timestamp.valueOf(date + " 00:00:00.000");
        Timestamp endTime = Timestamp.valueOf(date + " 23:59:59.999");

        //2. We recover the data from LHC Logging
        VariableSet variables = metaService.getVariablesWithNameInListofStrings(psenTags);
        TimeseriesDataSet dataSet = null;

        try {

            for (Variable var : variables) {

                dataSet = timeseriesDataService.getDataInTimeWindow(var, startTime, endTime);

                if (dataSet != null) {

                    for (TimeseriesData t : dataSet) {

                        tagValues.add(new TagValue(convertPsenNameToDatapointName(var.getVariableName()), t.getStamp(), t.getDoubleValue()));

                        logger.info("[" + t.getStamp() + "]" + t.getDoubleValue());

                        System.out.println("[" + t.getStamp() + "]" + t.getDoubleValue());

                    }

                }
            }
        } catch (NoSuchMethodException e) {

            logger.error(e.getMessage());

        }

        return tagValues;

    }

    /**
     * Get all the values for the given interval for the list of tags in the tagList
     * It can be used to import only one tag, too.
     * The tags must follow the PSEN name convention
     *
     * @param tagList
     * @return
     */
    public List<TagValue> importData(List<String> tagList, Date dayStart, Date dayEnd) {

        List<TagValue> tagValues = new ArrayList<>();

        String dateStart = sdf.format(dayStart);

        String dateEnd = sdf.format(dayEnd);

        List<String> psenTags = tagList
                .stream()
                .map(t -> convertTagNameToPsenName(t))
                .collect(Collectors.toList());


        // 1. Set the init time to import and the end time
        Timestamp startTime = Timestamp.valueOf(dateStart + " 00:00:00.000");
        Timestamp endTime = Timestamp.valueOf(dateEnd + " 23:59:59.999");

        //2. We recover the data from LHC Logging
        VariableSet variables = metaService.getVariablesWithNameInListofStrings(psenTags);
        TimeseriesDataSet dataSet = null;

        try {

            for (Variable var : variables) {

                dataSet = timeseriesDataService.getDataInTimeWindow(var, startTime, endTime);

                if (dataSet != null) {

                    for (TimeseriesData t : dataSet) {

                        tagValues.add(new TagValue(convertPsenNameToDatapointName(var.getVariableName()), t.getStamp(), t.getDoubleValue()));

                        logger.info("[" + t.getStamp() + "]" + t.getDoubleValue());

                        System.out.println("[" + t.getStamp() + "]" + t.getDoubleValue());

                    }

                }
            }
        } catch (NoSuchMethodException e) {

            logger.error(e.getMessage());

        }

        return tagValues;

    }



    /**
     * Get all the values for the given day for the given tag
     * The tag must follow the PSEN name convention
     * @param tag
     * @param day
     * @return
     */
    public List<TagValue> importDailyData(String tag, Date day) {

        List<String> tagList = new ArrayList<>();

        tagList.add(convertTagNameToPsenName(tag));

        return importDailyData(tagList, day);

    }


    /**
     * converts the tag name to PSEN ex: EMD204/1E_Ea- ----> EMD204_SLASH_1E_Ea_DASH_
     * @param name
     * @return
     */
    public static String convertTagNameToPsenName(String name){

        name = name.replaceAll("/", "_SLASH_");
        name = name.replaceAll("-", "_DASH_");
        name = name.replaceAll("\\*", "_STAR_");

        return name;
    }

    /**
     * converts the datapoint PSEN name to RTU name ex: EMD204_SLASH_1E_Ea_DASH_ ---->  EMD204/1E_Ea-
     * @param name
     * @return
     */
    public static String convertPsenNameToDatapointName(String name){

        name = name.replaceAll("_SLASH_", "/");
        name = name.replaceAll("_DASH_", "-");
        name = name.replaceAll("_STAR_", "\\*");

        return name;
    }


}

