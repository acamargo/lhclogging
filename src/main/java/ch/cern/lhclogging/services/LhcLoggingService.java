package ch.cern.lhclogging.services;

import ch.cern.lhclogging.model.TagValue;

import java.util.Date;
import java.util.List;

/**
 * Created by alejandrocamargo on 12/4/17.
 */

public interface LhcLoggingService {

    List<TagValue> importDailyData(List<String> tagList, Date day);

    List<TagValue> importDailyData(String tag, Date day);

    List<TagValue> importData(List<String> tagList, Date dayStart, Date dayEnd);

}
