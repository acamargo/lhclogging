This is a Spring MVC application to expose a REST API to get data from LHCLogging.

It uses the Timber extraction API provided in:

https://wikis.cern.ch/display/CALS/Extraction+API+changes

